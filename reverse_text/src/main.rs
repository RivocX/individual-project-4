use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use serde_json::json;

#[derive(Deserialize)]
struct CustomEvent {
    text: String,
}

#[derive(Serialize)]
struct CustomResponse {
    result: String,
}

async fn handle_event(event: CustomEvent, _: Context) -> Result<CustomResponse, Error> {
    let result = event.text.chars().rev().collect::<String>();
    Ok(CustomResponse { result })
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(handle_event)).await
}

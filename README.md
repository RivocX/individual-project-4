# Individual Project 4 
> Puyang(Rivoc) Xu (px16)

This is a Rust AWS Lambda and Step Functions project. There are two cargo project that will work together in the AWS Step Functions.

Demo video: ![](./media/demo.mov)

## Preparation
The installation of `Cargo` is needed in this project.
Create two new cargo projects:
```
cargo lambda new add_prefix_and_suffix
cargo lambda new reverse_text
```

Modify dependencies in `Cargo.toml`:
```
[dependencies]

lambda_runtime = "0.11.1"
serde = "1"
tokio = { version = "1", features = ["macros"] }
serde_json = "1.0"
```

Create a role in AWS IAM called ip4 with following policies: `AWSLambda_FullAccess`, `AWSLambdaBasicExecutionRole`, `IAMFullAccess`.

## Functions
The `add_prefix_and_suffix` function is designed to manipulate strings by appending predefined prefixes and suffixes to a given input text. This function takes three inputs: the main text, a prefix, and a suffix, and it concatenates them in order to produce a new string. This is particularly useful for formatting text in a consistent manner for logs, messages, or data tagging.

The `reverse_text` function performs a simple transformation on input text by reversing the order of its characters. This function is useful for text analysis, creating palindromes, or in scenarios where text obfuscation is needed. It accepts a single string input and outputs the reversed version of that string, thereby demonstrating a straightforward but effective text manipulation technique.

Then create a `input.json` file seperately and test each function.
```
cargo lambda watch
cargo lambda invoke --data-file input.json
```
![](./media/local1.png)

![](./media/local2.png)


## AWS
Deploy two functions to AWS seperately.
 ```
cargo lambda build --release
cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::339712859714:role/ip4
```
![](./media/lambda.png)

Test them separately on AWS Lambda.
![](./media/test1.png)

![](./media/test2.png)

Create a AWS Step Function based on their ARN using JSON code. Here I added a Pass state to process the result, map the result field to the text field, and then pass it to the `reverse_text` function.
```
{
  "Comment": "A text processing state machine",
  "StartAt": "AddPrefixSuffix",
  "States": {
    "AddPrefixSuffix": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-1:339712859714:function:add_prefix_and_suffix",
      "Next": "PrepareReverseInput"
    },
    "PrepareReverseInput": {
      "Type": "Pass",
      "InputPath": "$.result",
      "ResultPath": "$.text",
      "Next": "ReverseText"
    },
    "ReverseText": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-1:339712859714:function:reverse_text",
      "End": true
    }
  }
}
```
![](./media/step_function.png)

## Data Processing Pipeline
In this project, I've developed a data processing pipeline using AWS Lambda and Step Functions. The pipeline comprises two Lambda functions:

1. **Add Prefix and Suffix Function**: This Lambda function takes input text and appends specified prefixes and suffixes. It’s designed to manipulate the text by adding predefined strings at the beginning and the end.

2. **Reverse Text Function**: Following the first function, this Lambda function reverses the modified text from the previous step. Its primary role is to take the result from the "Add Prefix and Suffix Function" and reverse the characters in the string.

These two functions are orchestrated by AWS Step Functions, which manage the flow of data between them. The Step Functions workflow starts with the "Add Prefix and Suffix Function", processes the output, and passes it to the "Reverse Text Function". This structured workflow not only automates the process but also ensures that the operations are performed in the correct sequence.

## CI/CD
Create a `.gitlab-ci.yml` file.
Push it to gitlab for a CI/CD pipeline. You can check the status at the beginning of this README.

## Demo Video
![demo](./media/demo.mov)

## Results & Screenshots

### Step Function Architecture
![](./media/sf.png)

### Step Function Results
![](./media/r1.png)

![](./media/r2.png)

State view of the execution
![](./media/sv.png)


